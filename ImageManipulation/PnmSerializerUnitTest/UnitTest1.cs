﻿using System;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using ImageManipulation;

namespace PnmSerializerUnitTest
{
    [TestClass]
    public class UnitTest1
    {
        [TestMethod]
        public void TestPnmSerialize()
        {
            // Arrange
            Pixel p1 = new Pixel(0, 1, 2);
            Pixel p2 = new Pixel(3, 4, 5);
            Pixel p3 = new Pixel(6, 7, 8);
            Pixel p4 = new Pixel(9, 10, 11);
            Pixel p5 = new Pixel(11, 12, 13);
            Pixel p6 = new Pixel(14, 15, 16);
            Pixel p7 = new Pixel(17, 18, 19);
            Pixel p8 = new Pixel(10, 33, 23);

            Pixel[,] data = new Pixel[2, 4];

            data[0, 0] = p1;
            data[0, 1] = p2;
            data[0, 2] = p3;
            data[0, 3] = p4;
            data[1, 0] = p5;
            data[1, 1] = p6;
            data[1, 2] = p7;
            data[1, 3] = p8;

            Image image = new Image("# image metadata", 255, data);

            PnmSerializer ser = new PnmSerializer();

            // Act
            String serializedImage = ser.Serialize(image);

            // Assert
            Console.WriteLine(serializedImage);
        }

        [TestMethod]
        public void TestPnmParse()
        {
            // Arrange
            Pixel p1 = new Pixel(23, 40, 70);
            Pixel p2 = new Pixel(30, 80, 90);
            Pixel p3 = new Pixel(19, 50, 88);
            Pixel p4 = new Pixel(50, 88, 77);

            Pixel[,] data = new Pixel[2, 2];

            data[0, 0] = p1;
            data[0, 1] = p2;
            data[1, 0] = p3;
            data[1, 1] = p4;

            Image image = new Image("# image metadata", 255, data);

            PnmSerializer ser = new PnmSerializer();

            String serializedImage = ser.Serialize(image);

            // Act
            Image parsedImage = ser.Parse(serializedImage);

            // Assert
            Assert.AreEqual(image.MetaData, "# image metadata");
            Assert.AreEqual(image.MaxRange, 255);
            Assert.AreEqual(image[0, 0], p1);
            Assert.AreEqual(image[0, 1], p2);
            Assert.AreEqual(image[1, 0], p3);
            Assert.AreEqual(image[1, 1], p4);
        }

        [TestMethod]
        [ExpectedException(typeof(ArgumentException))]
        public void TestPnmParseWrongFormat()
        {
                Pixel p1 = new Pixel(23);
                Pixel p2 = new Pixel(30);
                Pixel p3 = new Pixel(19);
                Pixel p4 = new Pixel(50);

                Pixel[,] data = new Pixel[2, 2];

                data[0, 0] = p1;
                data[0, 1] = p2;
                data[1, 0] = p3;
                data[1, 1] = p4;

                Image image = new Image("#image metadata", 255, data);
                PgmSerializer pgmSer = new PgmSerializer();
                String serializedImage = pgmSer.Serialize(image);

                PnmSerializer pnmSer = new PnmSerializer();
                Image imageToParse = pnmSer.Parse(serializedImage);

            
        }
    }// End class
}

﻿using System;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using ImageManipulation;

namespace ImageUnitTestClass
{
    [TestClass]
    public class UnitTest1
    {
        [TestMethod]
        public void TestConPass()
        {
            //arrange
            Pixel p1 = new Pixel(23, 45, 7);
            Pixel p2 = new Pixel(30,50,10);
            Pixel[,] data = new Pixel[1, 2];

            data[0,0] = p1;
            data[0,1] = p2;

            //act
            Image image = new Image("# image", 255, data);

            //assert
            Assert.AreEqual(image.MetaData, "# image");
            Assert.AreEqual(image.MaxRange, 255);
            Assert.AreEqual(image[0, 0], p1);
            Assert.AreEqual(image[0, 1], p2);
        }

        [ExpectedException(typeof(ArgumentException))]
            [TestMethod]
            public void TestConMaxRangeNegative()
            {
                //arrange
                Pixel p1 = new Pixel(23, 45, 7);
                Pixel p2 = new Pixel(30, 50, 10);
                Pixel[,] data = new Pixel[1, 2];

                data[0, 0] = p1;
                data[0, 1] = p2;

                //act
                Image image = new Image("# image", -255, data);
            }



        [TestMethod]
        public void TestToGrey()
        {
            //arrange
            Pixel p1 = new Pixel(23, 45, 7);
            Pixel p2 = new Pixel(30, 50, 10);
            Pixel[,] data = new Pixel[1, 2];

            data[0, 0] = p1;
            data[0, 1] = p2;

            //act
            Image image = new Image("# image", 255, data);
            image.ToGrey();

            //assert
            Assert.AreEqual(image[0,1].Red,30);
            Assert.AreEqual(image[0,1].Green,30);
            Assert.AreEqual(image[0, 1].Blue,30);
        }


        [TestMethod]
        public void TestFlipHorizon()
        {
            //arrange
            Pixel p1 = new Pixel(0, 1, 2);
            Pixel p2 = new Pixel(3, 4, 5);
            Pixel p3 = new Pixel(6, 7, 8);
            Pixel p4 = new Pixel(9, 10, 11);
            Pixel p5 = new Pixel(11, 12, 13);
            Pixel p6 = new Pixel(14, 15, 16);
            Pixel p7 = new Pixel(17, 18, 19);
            Pixel p8 = new Pixel(10, 33, 23);



            Pixel[,] data = new Pixel[2, 4];

            data[0, 0] = p1;
            data[0, 1] = p2;
            data[0, 2] = p3;
            data[0, 3] = p4;
            data[1, 0] = p5;
            data[1, 1] = p6;
            data[1, 2] = p7;
            data[1, 3] = p8;

            bool horizon = true;

            //act
            Image image = new Image("# image", 255, data);
           image.Flip(horizon);//will flip horizontally
           
            //assert
            Assert.AreEqual(image[0, 0], p4);
            Assert.AreEqual(image[0, 1], p3);
            Assert.AreEqual(image[0, 2], p2);
            Assert.AreEqual(image[0, 3], p1);
            Assert.AreEqual(image[1, 0], p8);
            Assert.AreEqual(image[1, 1], p7);
            Assert.AreEqual(image[1, 2], p6);
            Assert.AreEqual(image[1, 3], p5);

        }


        
        [TestMethod]
        public void TestFlipVertical()
        {
            //arrange
            Pixel p1 = new Pixel(0, 1, 2);
            Pixel p2 = new Pixel(3, 4, 5);
            Pixel p3 = new Pixel(6, 7, 8);
            Pixel p4 = new Pixel(9, 10, 11);
            Pixel p5 = new Pixel(11, 12, 13);
            Pixel p6 = new Pixel(14, 15, 16);


            Pixel[,] data = new Pixel[2, 3];

            data[0, 0] = p1;
            data[0, 1] = p2;
            data[0, 2] = p3;
            data[1, 0] = p4;
            data[1, 1] = p5;
            data[1, 2] = p6;

            bool horizon = false;

            //act
            Image image = new Image("# image", 255, data);
           image.Flip(horizon);//will flip vertically

            //assert
            Assert.AreEqual(image[0, 0], p4);
            Assert.AreEqual(image[0, 1], p5);
            Assert.AreEqual(image[0, 2], p6);
            Assert.AreEqual(image[1, 0], p1);
            Assert.AreEqual(image[1, 1], p2);
            Assert.AreEqual(image[1, 2], p3);
    
        }


        [TestMethod]
        public void TestCrop()
        {
            //arrange
            Pixel p1 = new Pixel(0, 1, 2);
            Pixel p2 = new Pixel(3, 4, 5);
            Pixel p3 = new Pixel(6, 7, 8);
            Pixel p4 = new Pixel(9, 10, 11);
            Pixel p5 = new Pixel(11, 12, 13);
            Pixel p6 = new Pixel(14, 15, 16);


            Pixel[,] data = new Pixel[2, 3];

            data[0, 0] = p1;
            data[0, 1] = p2;
            data[0, 2] = p3;
            data[1, 0] = p4;
            data[1, 1] = p5;
            data[1, 2] = p6;

            //act
            Image image = new Image("# image", 255, data);
            image.Crop(0, 0, 1, 1);

            //assert
            Assert.AreEqual(image[0, 0], p1);
            Assert.AreEqual(image.Width, 1);
            Assert.AreEqual(image.Height, 1);
        }


        [ExpectedException(typeof(ArgumentException))]
        [TestMethod]
        public void TestCropLargerY()
        {
            //arrange
            Pixel p1 = new Pixel(0, 1, 2);
            Pixel p2 = new Pixel(3, 4, 5);
            Pixel p3 = new Pixel(6, 7, 8);
            Pixel p4 = new Pixel(9, 10, 11);
            Pixel p5 = new Pixel(11, 12, 13);
            Pixel p6 = new Pixel(14, 15, 16);


            Pixel[,] data = new Pixel[2, 3];

            data[0, 0] = p1;
            data[0, 1] = p2;
            data[0, 2] = p3;
            data[1, 0] = p4;
            data[1, 1] = p5;
            data[1, 2] = p6;

            //act
            Image image = new Image("# image", 255, data);
 
            image.Crop(0, 34536, 1, 1);
        }

        [ExpectedException(typeof(ArgumentException))]
        [TestMethod]
        public void TestCropLargerX()
        {
            //arrange
            Pixel p1 = new Pixel(0, 1, 2);
            Pixel p2 = new Pixel(3, 4, 5);
            Pixel p3 = new Pixel(6, 7, 8);
            Pixel p4 = new Pixel(9, 10, 11);
            Pixel p5 = new Pixel(11, 12, 13);
            Pixel p6 = new Pixel(14, 15, 16);


            Pixel[,] data = new Pixel[2, 3];

            data[0, 0] = p1;
            data[0, 1] = p2;
            data[0, 2] = p3;
            data[1, 0] = p4;
            data[1, 1] = p5;
            data[1, 2] = p6;

            //act
            Image image = new Image("# image", 255, data);
            image.Crop(3456, 0, 1, 1);
        }
        [ExpectedException(typeof(ArgumentException))]
        [TestMethod]
        public void TestCropNegative()
        {
            //arrange
            Pixel p1 = new Pixel(0, 1, 2);
            Pixel p2 = new Pixel(3, 4, 5);
            Pixel p3 = new Pixel(6, 7, 8);
            Pixel p4 = new Pixel(9, 10, 11);
            Pixel p5 = new Pixel(11, 12, 13);
            Pixel p6 = new Pixel(14, 15, 16);


            Pixel[,] data = new Pixel[2, 3];

            data[0, 0] = p1;
            data[0, 1] = p2;
            data[0, 2] = p3;
            data[1, 0] = p4;
            data[1, 1] = p5;
            data[1, 2] = p6;

            //act
            Image image = new Image("# image", 255, data);

            image.Crop(0, 0, -1, 1);
        }

        [ExpectedException(typeof(ArgumentException))]
        [TestMethod]
        public void TestCropBigStart()
        {
            //arrange
            Pixel p1 = new Pixel(0, 1, 2);
            Pixel p2 = new Pixel(3, 4, 5);
            Pixel p3 = new Pixel(6, 7, 8);
            Pixel p4 = new Pixel(9, 10, 11);
            Pixel p5 = new Pixel(11, 12, 13);
            Pixel p6 = new Pixel(14, 15, 16);


            Pixel[,] data = new Pixel[2, 3];

            data[0, 0] = p1;
            data[0, 1] = p2;
            data[0, 2] = p3;
            data[1, 0] = p4;
            data[1, 1] = p5;
            data[1, 2] = p6;

            //act
            Image image = new Image("# image", 255, data);

            image.Crop(10, 0, 1, 1);
        }
    }
}
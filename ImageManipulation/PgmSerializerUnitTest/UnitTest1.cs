﻿using System;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using ImageManipulation;

namespace PgmSerializerUnitTest
{
    [TestClass]
    public class UnitTest1
    {
        [TestMethod]
        public void TestPgmSerialize()
        {
            // Arrange
            Pixel p1 = new Pixel(23);
            Pixel p2 = new Pixel(30);
            Pixel p3 = new Pixel(19);
            Pixel p4 = new Pixel(50);

            Pixel[,] data = new Pixel[2, 2];

            data[0, 0] = p1;
            data[0, 1] = p2;
            data[1, 0] = p3;
            data[1, 1] = p4;

            Image image = new Image("#image metadata", 255, data);

            PgmSerializer ser = new PgmSerializer();

            // Act
            String serializedImage = ser.Serialize(image);

            // Assert
            Console.WriteLine(serializedImage);
        }

        [TestMethod]
        public void TestPgmParse()
        {
            // Arrange
            Pixel p1 = new Pixel(23);
            Pixel p2 = new Pixel(30);
            Pixel p3 = new Pixel(19);
            Pixel p4 = new Pixel(50);

            Pixel[,] data = new Pixel[2, 2];

            data[0, 0] = p1;
            data[0, 1] = p2;
            data[1, 0] = p3;
            data[1, 1] = p4;

            Image image = new Image("# image metadata", 255, data);

            PgmSerializer ser = new PgmSerializer();

            String serializedImage = ser.Serialize(image);

            // Act
            Image parsedImage = ser.Parse(serializedImage);

            // Assert
            Assert.AreEqual(image.MetaData, "# image metadata");
            Assert.AreEqual(image.MaxRange, 255);
            Assert.AreEqual(image[0, 0], p1);
            Assert.AreEqual(image[0, 1], p2);
            Assert.AreEqual(image[1, 0], p3);
            Assert.AreEqual(image[1, 1], p4);
        }

        [TestMethod]
        [ExpectedException(typeof(ArgumentException))]
        public void TestPgmParseWrongFormat()
        {
                // Arrange
                Pixel p1 = new Pixel(23);
                Pixel p2 = new Pixel(30);
                Pixel p3 = new Pixel(19);
                Pixel p4 = new Pixel(50);

                Pixel[,] data = new Pixel[2, 2];

                data[0, 0] = p1;
                data[0, 1] = p2;
                data[1, 0] = p3;
                data[1, 1] = p4;

                Image image = new Image("# image metadata", 255, data);
                PgmSerializer pgmSer = new PgmSerializer();
                String serializedImage = pgmSer.Serialize(image);

                PnmSerializer pnmSer = new PnmSerializer();
                Image imageToParse = pnmSer.Parse(serializedImage);
           
        }

    }// End class
}

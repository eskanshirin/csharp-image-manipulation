﻿using System;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using ImageManipulation;

namespace TempImageManip
{
    [TestClass]
    public class UnitTest1
    {
        [TestMethod]
        public void TestCats()
        {
            Image[] images = ImageUtilities.LoadFolder(@".\Cats");
            Image[] modifiedImg = new Image[1];

            // ToGrey
            //images[0].ToGrey(); 

            //modifiedImg[0] = images[0];

            //ImageUtilities.SaveFolder(@".\ModifiedCats", modifiedImg, "pnm");

            // Flip
            Boolean horizontal = true;
            images[0].Flip(horizontal);

            modifiedImg[0] = images[0];

            ImageUtilities.SaveFolder(@".\ModifiedCats", modifiedImg, "pnm");

            //images[0].Crop(0, 0, 100, 100);

            //modifiedImg[0] = images[0];

            //ImageUtilities.SaveFolder(@".\ModifiedCats", modifiedImg, "pnm");
        }
    }
}

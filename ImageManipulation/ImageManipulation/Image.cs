﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;


namespace ImageManipulation
{
    public class Image
    {
        private String metaData;//info about the Image
        private int maxRange; //maxValeu of the pixle
        private Pixel[,] data;

        public String MetaData
        {
            get
            {
                return this.metaData;
            }
        }

        public int MaxRange
        {
            get
            {
                return this.maxRange;
            }
        }

        public Pixel this[int index1,int index2]
        {
           get { return this.data[index1,index2];}
        }
       
        //helper methods used in ser.files

        public int Width
        {
            get { return this.data.GetLength(1); }
        }

        public int Height
        {
            get { return this.data.GetLength(0); }
        }


        public Image(String metaData, int maxRange, Pixel[,] data)
        {
            this.metaData = metaData;
            if (maxRange < 0)
            {
                throw new ArgumentException("Can't be negative");
            }
            this.maxRange = maxRange;

            //initialize the this.data array
            this.data = new Pixel[data.GetLength(0), data.GetLength(1)];

            for (int row = 0; row < data.GetLength(0); row++)
            {
                for (int col = 0; col < data.GetLength(1); col++)
                {
                    this.data[row, col] = data[row, col];
                }
            }
        }//end constructor


        public void ToGrey()
        {
            for (int row = 0; row < data.GetLength(0); row++)
            {
                for (int col = 0; col < data.GetLength(1); col++)
                {
                    int num = this.data[row, col].Grey();
                    this.data[row, col] = new Pixel(num); //would call the intensity con in Pixle
                }
            }

        }//end method 

        public void Flip(bool horizon)
        {
            if (horizon)
            {
                FlipHorizon();
            }
            else
            {
                FlipVertical();
            }

        }
        public void FlipHorizon()
        {
            Pixel[,] temp = new Pixel[this.data.GetLength(0), this.data.GetLength(1)];
            //row=index0 that is the unchangable 
            int col;
            for (int index = 0; index < this.data.GetLength(0); index++)
            {
                col= this.data.GetLength(1) - 1;//every index iteration must reinitialize itself
                for (int index2 = 0; index2 < this.data.GetLength(1); index2++)
                {
                    temp[index, index2] = this.data[index, col];

                    col--;
                }
            }//end big for
            this.data = temp;
        }//end method
 

        public void FlipVertical()
        {
         int row = this.data.GetLength(0);
         int col = this.data.GetLength(1);
         Pixel[,] temp = new Pixel[row, col];

         int tempRow = this.data.GetLength(0) - 1;

        for (int i = 0; i < row; i++)
        {
          for (int j = 0; j < col; j++)
          {

          temp[tempRow, j] = data[i, j];

          }
         tempRow--;
        }
            this.data = temp;
        }

            public void Crop(int startX, int startY, int endX, int endY) //end is exclusive
        {
            //validation
            if (startX < 0 || startY < 0 || endX < 0 || endY < 0)
            {
                throw new ArgumentException("Index can't be negative");
            }

            if (startX > startY || startY > endY)
            {
                throw new ArgumentException("Start can't be bigger than 0");
            }

            if (endX > this.maxRange || endY > this.maxRange)
            {
                throw new ArgumentException("There is a limit");
            }

            int row = 0;
            Pixel[,] temp = new Pixel[endX - startX, endY - startY]; 
            for (int vertical = startY; vertical < endY; vertical++)
            {
                int col = 0;
                for (int horizon = startX; horizon < endX; horizon++)
                {
                    temp[row, col] = this.data[vertical, horizon];
                    col++;
                }
                row++;

            }//end for
            this.data = temp; //copy ref.
        }//end method

    }//class
}//nameSpace

﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ImageManipulation
{
    public class PgmSerializer : IImageSerializer
    {
        public String Serialize(Image i)
        {
            // Add format
            String serializedImage = "P2" + Environment.NewLine;

            // Add 1 or more lines of metadata 
            serializedImage += i.MetaData + Environment.NewLine;

            // Add dimensions
            int width = i.Width;
            int height = i.Height;
            serializedImage += i.Width.ToString() + " " + i.Height.ToString() + Environment.NewLine;

            // Add maxValue
            serializedImage += i.MaxRange.ToString() + Environment.NewLine;

            // Serialize pixel numbers in String format
            String pixelString = StorePixels(i);

            // Add pixel numbers
            serializedImage += pixelString;

            return serializedImage;
        }

        public Image Parse(String imageData)
        {
            Image parsedImage;

            // Create array of lines ***********************
            String[] arrayElements = imageData.Split(Environment.NewLine.ToCharArray(), StringSplitOptions.RemoveEmptyEntries);

            String format = arrayElements[0];
            if ( !(format.Equals("P2")) )
            {
                throw new ArgumentException("The data does not have the right format");
            }

            // Get metadata
            String metaData = "";
            int indexMetaData = 1;
            for (int i = 1; i < arrayElements.Length; i++)
            {
                if (arrayElements[i].Contains("#"))
                {
                    metaData = arrayElements[i] + Environment.NewLine;
                    indexMetaData++;
                }
                else
                {
                    break;
                }
            }

            // Get dimensions
            String[] heightAndWidth = arrayElements[indexMetaData].Split(' ');
            int width = Int32.Parse(heightAndWidth[0]);
            int height = Int32.Parse(heightAndWidth[1]);

            // Get maxValue
            int maxValue = Int32.Parse(arrayElements[indexMetaData + 1]);

            // Get rectangular array of Pixels
            Pixel[,] pixels = ExtractPixels(arrayElements, indexMetaData + 2, height, width);

            parsedImage = new Image(metaData, maxValue, pixels);

            return parsedImage;
        }

        public String StorePixels(Image i)
        {
            String pixelString = "";

            for (int row = 0; row < i.Height; row++)
            {
                for (int col = 0; col < i.Width; col++)
                {
                    pixelString += i[row, col].Red.ToString() + Environment.NewLine;
                }
            }

            return pixelString;
        }

        public Pixel[,] ExtractPixels(String[] arrayElements, int index, int height, int width)
        {
            Pixel[,] pixels = new Pixel[height, width];

            for (int i = 0; i < height; i++)
            {
                for(int j = 0; j < width; j++)
                {
                    Pixel pixel = new Pixel(Int32.Parse(arrayElements[index]));
                    index++;

                    pixels[i, j] = pixel;
                }   
            }

            return pixels;
        }

    }// End class
}
﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ImageManipulation
{
    public class ImageUtilities
    {
        public static Image[] LoadFolder(String dirPath)
        {
            if ( !Directory.Exists(dirPath) )
            {
                Directory.CreateDirectory(dirPath);
            }

            String[] fileNames = Directory.GetFiles(dirPath);

            // Count number of pnm and pgm files
            int numImages = 0;
            for (int i = 0; i < fileNames.Length; i++)
            {
                String[] extension = fileNames[i].Split('.');
                if (extension[extension.Length - 1].Equals("pnm") || extension[extension.Length - 1].Equals("pgm"))
                {
                    numImages++;
                }
            }

            Image[] images = new Image[numImages];

            int count = 0;
            for (int i = 0; i < fileNames.Length; i++)
            {
                String[] extension = fileNames[i].Split('.');
                if (extension[extension.Length -1].Equals("pnm"))
                {
                    PnmSerializer pnmSer = new PnmSerializer();

                    // Read from the file
                    String imageData = File.ReadAllText(fileNames[i]);

                    // Create an Image with this data
                    Image image = pnmSer.Parse(imageData);

                    // Add the image to the array
                    images[count] = image;
                    count++;
                }

                if (extension[extension.Length - 1].Equals("pgm"))
                {
                    //StreamReader reader = new StreamReader(fileNames[i]);
                    //String imageData = reader.ReadToEnd();

                    PgmSerializer pgmSer = new PgmSerializer();

                    // Read from the file
                    String imageData = File.ReadAllText(fileNames[i]);

                    // Create an Image with this data
                    Image image = pgmSer.Parse(imageData);

                    // Add the image to the array
                    images[count] = image;
                    count++;
                }
            }

            return images;
        } // End LoadFolder method

        public static void SaveFolder (String path, Image[] images, String format)
        {
            if (!Directory.Exists(path))
            {
                Directory.CreateDirectory(path);
            }

            // Serialize the images
            String[] serializedImages = new String[images.Length];

            if (format.Equals("pgm"))
            {
                PgmSerializer pgmSer = new PgmSerializer();

                for (int i = 0; i < images.Length; i++)
                {
                    serializedImages[i] = pgmSer.Serialize(images[i]);
                }
            }
            else
            {
                PnmSerializer pnmSer = new PnmSerializer();

                for (int i = 0; i < images.Length; i++)
                {
                    serializedImages[i] = pnmSer.Serialize(images[i]);
                }
            }

            // Create image files
            for (int i = 0; i < images.Length; i++)
            {
                // Create a file
                String imagePath = path + "\\" + "image" + i.ToString() + "." + format;
                StreamWriter writer = new StreamWriter(imagePath);
                writer.Close();
            }

            // Write each image onto a file
            for (int i = 0; i < images.Length; i++)
            {
                String imagePath = path + "\\" + "image" + i.ToString() + "." + format;
                StreamWriter writer = new StreamWriter(imagePath);
                writer.Write(serializedImages[i]);
                writer.Close();
            }
        }
    }// End class
}

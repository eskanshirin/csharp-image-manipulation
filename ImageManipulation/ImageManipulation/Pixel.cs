﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ImageManipulation
{
    public class Pixel
    {
        private int red;
        private int green;
        private int blue;

        public Pixel(int red1, int green1, int blue1)
        {
            if (red1 < 0 || green1 < 0 || blue1 < 0 || red1 > 255 || green1 > 255 || blue1 > 255)
            {
                throw new ArgumentException("input should be between 0-255");
            }
            this.red = red1;
            this.green = green1;
            this.blue = blue1;

        }
        public Pixel(int intensity)
        {
            if (intensity < 0 || intensity > 255)
            {
                throw new ArgumentException("input should be between 0-255");
            }
            this.red = intensity;
            this.green = intensity;
            this.blue = intensity;
        }

        public int Red
        {
            get { return this.red; }
            private set { this.red = value; }
        }
        public int Green
        {
            get { return green; }
            private set { green = value; }
        }
        public int Blue
        {
            get { return blue; }
            private set { blue = value; }
        }
        public int Grey()
        {
            return (int)((red + blue + green) / 3);

        }
    }
}

﻿using System;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using ImageManipulation;

namespace PixelUnitTest
{
    [TestClass]
    public class UnitTest1
    {
        
        [TestMethod]
        public void TestConstructorPass()
        {
            //arrange // act
            Pixel passed = new Pixel(255, 0, 0);
            //assert
            Assert.AreEqual(passed.Green, 0);
            Assert.AreEqual(passed.Red, 255);
            Assert.AreEqual(passed.Blue, 0);
        
        }
      
        [TestMethod]
        [ExpectedException(typeof(ArgumentException))]
        public void TestConstructorFailNegativeNumber()
        {
       
                //arrange // act
                Pixel failed = new Pixel(-255, 0, 0);
           
        }
        
        [TestMethod]
        [ExpectedException(typeof(ArgumentException))]
        public void TestConstructorFailMaxNumber()
        {
          
                //arrange // act
                Pixel failed = new Pixel(2565, 0, 0);
           
           
        }

    
    [TestMethod]
        public void TestConstructorIntensityPass()
        {

            //arrange // act
            Pixel failed = new Pixel(222);
            //assert
            Assert.AreEqual(failed.Red, 222);
            Assert.AreEqual(failed.Blue, 222);
            Assert.AreEqual(failed.Green, 222);
        }



        [TestMethod]
        [ExpectedException(typeof(ArgumentException))]
        public void TestConstructorIntensityNegative()
        {


            //arrange // act
            Pixel failed = new Pixel(-345);



        }


        [TestMethod]
        [ExpectedException(typeof(ArgumentException))]
        public void TestConstructorIntensityMaxNumber()
        {

            //arrange // act
            Pixel failed = new Pixel(4563453);
        }
   


    [TestMethod]
        public void toGreyPass()
        {

            //arrange 
            Pixel passed = new Pixel(0, 10, 20);
            int avrage = 0;
            //act
            avrage = passed.Grey();
            //assert
            Assert.AreEqual(avrage, 10);

        }
    }//end class
}//end 
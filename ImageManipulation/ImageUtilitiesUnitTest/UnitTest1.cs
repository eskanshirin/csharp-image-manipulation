﻿using System;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using ImageManipulation;

namespace ImageUtilitiesUnitTest
{
    [TestClass]
    public class UnitTest1
    {
        [TestMethod]
        public void LoadFolderTest()
        {
            // Arrange
            Pixel p1 = new Pixel(23, 40, 70);
            Pixel p2 = new Pixel(30, 80, 90);
            Pixel p3 = new Pixel(18, 50, 33);
            Pixel p4 = new Pixel(50, 88, 55);

            Pixel[,] data1 = new Pixel[2, 2];

            data1[0, 0] = p1;
            data1[0, 1] = p2;
            data1[1, 0] = p3;
            data1[1, 1] = p4;

            Image image1 = new Image("# image1 metadata", 255, data1);

            Pixel p11 = new Pixel(23, 40, 70);
            Pixel p22 = new Pixel(30, 80, 90);
            Pixel p33 = new Pixel(19, 50, 88);
            Pixel p44 = new Pixel(50, 88, 77);

            Pixel[,] data2 = new Pixel[2, 2];

            data2[0, 0] = p1;
            data2[0, 1] = p2;
            data2[1, 0] = p3;
            data2[1, 1] = p4;

            Image image2 = new Image("# image2 metadata", 255, data2);

            Image[] images = new Image[2];
            images[0] = image1;
            images[1] = image2;

            ImageUtilities.SaveFolder(@".\SavedImages", images, "pnm");

            // Act
            Image[] imagesArray = ImageUtilities.LoadFolder(@".\SavedImages");

            PnmSerializer pnmSer = new PnmSerializer();
            String[] serializedImages = new String[imagesArray.Length];

            // Assert
            for (int i = 0; i < imagesArray.Length; i++)
            {
                serializedImages[i] = pnmSer.Serialize(imagesArray[i]);
            }

            for (int i = 0; i < serializedImages.Length; i++)
            {
                Console.WriteLine(serializedImages[i]);
            }
        }

        [TestMethod]
        public void SaveFolderTest()
        {
            // Arrange
            Pixel p1 = new Pixel(23, 40, 70);
            Pixel p2 = new Pixel(30, 80, 90);
            Pixel p3 = new Pixel(18, 50, 33);
            Pixel p4 = new Pixel(50, 88, 55);

            Pixel[,] data1 = new Pixel[2, 2];

            data1[0, 0] = p1;
            data1[0, 1] = p2;
            data1[1, 0] = p3;
            data1[1, 1] = p4;

            Image image1 = new Image("# image1 metadata", 255, data1);
            
            Pixel p11 = new Pixel(23, 40, 70);
            Pixel p22 = new Pixel(30, 80, 90);
            Pixel p33 = new Pixel(19, 50, 88);
            Pixel p44 = new Pixel(50, 88, 77);

            Pixel[,] data2 = new Pixel[2, 2];

            data2[0, 0] = p1;
            data2[0, 1] = p2;
            data2[1, 0] = p3;
            data2[1, 1] = p4;

            Image image2 = new Image("# image2 metadata", 255, data2);

            
            Image[] images = new Image[2];
            images[0] = image1;
            images[1] = image2;

            // Act, Assert
            ImageUtilities.SaveFolder(@".\SavedImages", images, "pnm");
        }

    }// End class
}
